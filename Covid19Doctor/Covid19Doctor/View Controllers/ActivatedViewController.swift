//
//  ActivatedViewController.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 11/03/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit
import PMSuperButton

class ActivatedViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var button: PMSuperButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customize()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    @IBAction func buttonTapped(_ sender: Any) {
        let vc = UIStoryboard.getViewController(id: "HomeViewController")
        let nc = UINavigationController(rootViewController: vc)
        UIApplication.shared.keyWindow?.rootViewController = nc
    }
}
