//
//  ActivatedViewCustomization.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension ActivatedViewController: Customizable {
    func customize() {
        titleLabel.map {
            $0.font = .title
            $0.textColor = .titleBlack
            $0.text = NSLocalizedString("all_set", comment: "")
        }
        
        textLabel.map {
            $0.font = .subtitle
            $0.textColor = .titleBlack
            $0.text = NSLocalizedString("what_to_do", comment: "")
        }
        
        button.map {
            $0.titleLabel?.font = .button
            $0.setTitleColor(.white, for: .normal)
            $0.backgroundColor = .mainTheme
            $0.setTitle(NSLocalizedString("go", comment: ""), for: .normal)
        }
    }
}
