//
//  HomeViewCustomization.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension HomeViewController: Customizable {
    func customize() {
        titleLabel.map {
            $0.font = .title
            $0.textColor = .titleBlack
            
            let text = NSLocalizedString("main_title", comment: "")
            let str = "Covi"
            let range = (text as NSString).range(of: str)
            let attributed = NSMutableAttributedString(string: text)
            attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainTheme, range: range)
            $0.attributedText = attributed
        }
        
        qrTextLabel.text = NSLocalizedString("bt_scan_qrcode", comment: "")
        qrTextLabel.font = UIFont(name: "SFCompactDisplay-Semibold", size: 20)
        qrTextLabel.textColor = .white
        
        enterButton.map {
            $0.setTitle(NSLocalizedString("bt_update_status", comment: ""), for: .normal)
            $0.titleLabel?.font = .button
            $0.setTitleColor(.white, for: .normal)
            $0.backgroundColor = .mainTheme
        }
        
        inviteButton.map {
            $0.setTitle(NSLocalizedString("bt_invite_doc", comment: ""), for: .normal)
            $0.titleLabel?.font = .button
            $0.setTitleColor(.mainTheme, for: .normal)
            $0.backgroundColor = .white
            $0.layer.borderColor = UIColor.mainTheme.cgColor
            $0.layer.borderWidth = 1
        }
    }
}
