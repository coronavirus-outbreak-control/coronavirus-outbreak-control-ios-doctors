//
//  Customizable.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import Foundation

protocol Customizable {
    func customize()
}
