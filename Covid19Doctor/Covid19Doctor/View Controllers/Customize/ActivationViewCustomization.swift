//
//  CustomActivation.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension ActivationViewController: Customizable {
    func customize() {
        phoneField.placeholder = NSLocalizedString("phone_number", comment: "")
        phoneField.withFlag = true
        phoneField.withPrefix = true
        if #available(iOS 11.0, *) {
            phoneField.withDefaultPickerUI = true
        }
        
        titleLabel.font = .title
        titleLabel.textColor = .titleBlack
        titleLabel.text = NSLocalizedString("verify_number", comment: "")
        
        textLabel.font = .caption
        textLabel.textColor = .textGray
        
        retryLabel.font = UIFont(name: "SFCompactDisplay-Regular", size: 15)
        retryLabel.textColor = .textGray
        
        codeTextLabel.text = NSLocalizedString("tv_code", comment: "")
        
        lineView.backgroundColor = .mainTheme
        
        phoneField.tintColor = .mainTheme
        
        sendPhoneButton.titleLabel?.font = .button
        sendPhoneButton.setTitleColor(.white, for: .normal)
        sendPhoneButton.backgroundColor = .mainTheme
        sendPhoneButton.setTitle(NSLocalizedString("bt_send_number", comment: ""), for: .normal)
    }
}
