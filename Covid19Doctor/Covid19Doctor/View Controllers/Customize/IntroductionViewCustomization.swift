//
//  IntroductionViewCustomization.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension IntroductionViewController: Customizable {
    func customize() {
        titleLabel.map {
            $0.font = .title
            $0.textColor = .titleBlack
            
            let text = NSLocalizedString("welcome", comment: "")
            // set inset as a workaround
            // for miscalculation of FittabbleFontLabel
            $0.leftInset = -1
            $0.rightInset = -1
            let str = "CovidCommunity"
            let range = (text as NSString).range(of: str)
            let attributed = NSMutableAttributedString(string: text)
            attributed.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainTheme, range: range)
            $0.attributedText = attributed
        }
        
        subtitleLabel.map {
            $0.font = .subtitle
            $0.textColor = .textGray
            $0.text = NSLocalizedString("together_we", comment: "")
        }
        
        continueButton.map {
            $0.titleLabel?.font = .button
            $0.setTitleColor(.white, for: .normal)
            $0.backgroundColor = .mainTheme
            $0.setTitle(NSLocalizedString("let_s_get_s", comment: ""), for: .normal)
        }
        
        howToButton.map {
            $0.setTitle(NSLocalizedString("bt_how_it_works", comment: ""), for: .normal)
            $0.titleLabel?.font = UIFont(name: "SFCompactDisplay-Semibold", size: 15)
            $0.setTitleColor(.titleBlack, for: .normal)
        }
    }
}
