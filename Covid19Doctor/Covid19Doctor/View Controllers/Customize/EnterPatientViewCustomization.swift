//
//  EnterPatientViewCustomization.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension EnterPatientViewController: Customizable {
    func customize() {
        titleLabel.map {
            $0.text = NSLocalizedString("enter_patient_title", comment: "")
            $0.textColor = .titleBlack
            $0.font = .title
        }
        
        idField.map {
            $0.placeholder = NSLocalizedString("patient_id", comment: "")
            $0.textColor = .titleBlack
            $0.font = UIFont(name: "SFCompactDisplay-Regular", size: 26)
        }
        
        continueButton.map {
            $0.setTitle(NSLocalizedString("continue", comment: "").uppercased(),
                        for: .normal)
            $0.setTitleColor(.white, for: .normal)
            $0.titleLabel?.font = .button
            $0.backgroundColor = .mainTheme
        }
        
        backButton.map {
            $0.backgroundColor = .mainTheme
            $0.imageEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
            $0.tintColor = .white
        }
        
        lineView.backgroundColor = .mainTheme
        
        dropDown.anchorView = idField
        dropDown.direction = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)! * 2.3)
        dropDown.cellHeight = 56
        dropDown.textFont = UIFont(name: "SFCompactDisplay-Regular", size: 20)!
    }
}
