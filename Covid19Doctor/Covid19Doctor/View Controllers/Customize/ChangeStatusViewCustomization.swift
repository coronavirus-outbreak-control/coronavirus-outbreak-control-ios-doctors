//
//  ChangeStatusViewCustomization.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit

extension ChangeStatusViewController: Customizable {
    func customize() {
        positiveButton.map {
            $0.backgroundColor = .statusPositive
            $0.setTitle(NSLocalizedString("bt_confirm_covid", comment: ""), for: .normal)
        }
        
        negativeButton.map {
            $0.backgroundColor = .statusNegative
            $0.setTitle(NSLocalizedString("bt_negative_covid", comment: ""), for: .normal)
        }
        
        healedButton.map {
            $0.backgroundColor = .statusHealed
            $0.setTitle(NSLocalizedString("bt_recover_covid", comment: ""), for: .normal)
        }
        
        [positiveButton, negativeButton, healedButton].forEach {
            $0?.titleLabel?.font = .button
            $0?.setTitleColor(.white, for: .normal)
        }
        
        textLabel.font = UIFont.title
        textLabel.textColor = .titleBlack
        textLabel.text = NSLocalizedString("diagnosis_title", comment: "")
        
        backButton.map {
            $0.backgroundColor = .mainTheme
            $0.imageEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
            $0.tintColor = .white
        }
    }
}
