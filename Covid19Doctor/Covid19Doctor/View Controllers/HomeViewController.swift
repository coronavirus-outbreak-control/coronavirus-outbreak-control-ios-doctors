//
//  HomeViewController.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 11/03/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit
import PMSuperButton
import ContactsUI
import RxSwift
import RxCocoa
import RxGesture
import Toast_Swift
import PhoneNumberKit

class HomeViewController: UIViewController {

    @IBOutlet weak var scanView: UIView!
    @IBOutlet weak var enterButton: PMSuperButton!
    @IBOutlet weak var inviteButton: PMSuperButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var qrTextLabel: UILabel!
    
    private let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customize()
        
        scanView.rx.tapGesture()
        .skip(1)
        .subscribe(onNext: { [weak self] _ in
            self?.launchScanner()
        })
        .disposed(by: bag)
        
        enterButton.rx.tap
            .subscribe(onNext: { [weak self] _ in
                self?.launchEnterPatient()
            })
            .disposed(by: bag)

        inviteButton.rx.tap
        .subscribe(onNext: { [weak self] _ in
            self?.launchContactPicker()
        })
        .disposed(by: bag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // recalculate corner radius for buttons because
        // the size of the buttons may change for smaller screens
        let height = enterButton.bounds.height
        [enterButton, inviteButton].forEach {
            $0?.cornerRadius = height/2
        }
    }
    
    // MARK: - Scanner
    
    let scanner = Scanner()
    private func launchScanner() {
        CameraPermission.check(from: self)
            .observeOn(MainScheduler.instance)
            .subscribe(onSuccess: { [weak self] permissionGranted in
                if permissionGranted, let `self` = self {
                    self.scanner.present(from: self)
                }
            })
            .disposed(by: bag)
    }
    
    // MARK: - Enter manually
    
    private func launchEnterPatient() {
        let vc = UIStoryboard.getViewController(id: "EnterPatientViewController")
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Invite
    
    private func launchContactPicker() {
        let contactPicker = CNContactPickerViewController()
        contactPicker.delegate = self
        // return only contacts with phone numbers
        contactPicker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
        // return only phone numbers
        contactPicker.displayedPropertyKeys = ["phoneNumbers"]
        present(contactPicker, animated: true)
    }
    
    private func inviteContact(_ contact: Contact, phoneNumber: String) {
        view.makeToastActivity(.center)
        
        guard let reAuthToken: String = Database.shared.getAccountValue(key: .reAuthToken) else {
            //TODO: handle error Errors.userNotActivated
            return
        }
        
        APIManager.api.runAuthenticated(reAuthToken: reAuthToken, apiBuilder: {
            APIManager.api.inviteDoctor(number: phoneNumber)
        })
        .observeOn(MainScheduler.instance)
        .subscribe(onSuccess: { [weak self] _ in
            self?.storeInvitation(name: contact.fullName, phoneNumber: phoneNumber)
            self?.view.hideToastActivity()
            self?.view.makeToast("\(NSLocalizedString("toast_num_doc_invited", comment: "")): \(contact.fullName)")
        }, onError: { [weak self] _ in
            self?.view.hideToastActivity()
            self?.view.makeToast("\(NSLocalizedString("toast_err_doc_invited", comment: "")) \(contact.fullName)")
        })
        .disposed(by: bag)
    }
    
    private func promptInvitation(contact: Contact, phoneNumber: String) {
        // phoneNumber is already validated
        let alertController = UIAlertController(title: "\(NSLocalizedString("alert_inviting_title", comment: ""))\n\(phoneNumber)", message: "\(NSLocalizedString("alert_inviting_text", comment: ""))", preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: NSLocalizedString("no", comment: ""), style: .cancel) { [weak self] _ in
            self?.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(cancelAction)

        let okAction = UIAlertAction(title: NSLocalizedString("yes", comment: ""), style: .default) { [weak self] _ in
            self?.inviteContact(contact, phoneNumber: phoneNumber)
        }
        alertController.addAction(okAction)

        present(alertController, animated: true)
        alertController.view.tintColor = .mainTheme
    }
    
    private func storeInvitation(name: String, phoneNumber: String) {
        let realm = Database.shared.realm()
        let invitation = InvitationObject()
        invitation.phoneNumber = phoneNumber
        invitation.contactName = name
        try! realm.write {
            realm.add(invitation)
        }
    }
    
    // MARK: - Invitations
    
    private func launchInvitations() {
        let vc = UIStoryboard.getViewController(id: "InvitationsViewController") as! InvitationsViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HomeViewController: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contactProperty: CNContactProperty) {
        if let phoneNumber = contactProperty.value as? CNPhoneNumber,
            let contact = Contact(contact: contactProperty.contact) {
            
            picker.dismiss(animated: true) { [weak self] in
                self?.handlePhoneValidation(for: contact, phoneNumber: phoneNumber.stringValue)
            }
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    private func handlePhoneValidation(for contact: Contact, phoneNumber: String) {
        if let number = PhoneNumberKit().validatedPhoneNumber(string: phoneNumber) {
            self.promptInvitation(contact: contact, phoneNumber: number)
        }
        else {
            promptWrongNumberAlert(for: contact, phoneNumber: phoneNumber)
        }
    }
    
    private func promptWrongNumberAlert(for contact: Contact, phoneNumber: String) {
        self.view.hideToastActivity()
        
        let alert = UIAlertController(title: NSLocalizedString("toast_err_doc_invited", comment: ""),
                                      message: NSLocalizedString("toast_err_doc_description", comment: ""),
                                      preferredStyle: UIAlertController.Style.alert )
        
        alert.addTextField { (textField) in
            textField.placeholder = NSLocalizedString("phone_number", comment: "")
            textField.keyboardType = .numberPad
            textField.text = phoneNumber
        }

        let submit = UIAlertAction(title: NSLocalizedString("toast_err_doc_description_try", comment: ""),
                                 style: .default) { (alertAction) in

            let textField = alert.textFields![0] as UITextField
                                    
            if let text = textField.text, text != "" {
                self.handlePhoneValidation(for: contact, phoneNumber: text)
            }
        }

        alert.addAction(submit)

        let cancel = UIAlertAction(title: NSLocalizedString("cancel", comment: ""),
                                   style: .default) { (alertAction) in }
        
        alert.addAction(cancel)
        self.present(alert, animated:true, completion: nil)

    }
}
