//
//  Scanner.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 04/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit
import RxSwift
import Toast_Swift

class Scanner {
    
    private let bag = DisposeBag()
    
    func present(from: UIViewController) {
        let scannerVC = UIStoryboard.getViewController(id: "ScannerViewController") as! ScannerViewController
        let scannerNC = UINavigationController(rootViewController: scannerVC)
        from.present(scannerNC, animated: true, completion: nil)
        
        scannerVC.output
        .flatMap({ CodeParser.extractPatientId(string: $0) })
        .subscribe({ [weak self, weak from] event in
            guard let from = from else { return }
            
            switch event {
            case .next(let patientId):
                // 'pending' state has been set on server
                self?.launchPatientReviewViewController(patientId: patientId, from: from)
            case .error(_):
                // TODO: show error (no error actually)
                break
            default:
                break
            }
        })
        .disposed(by: bag)
    }
    
    func launchPatientReviewViewController(patientId: String, from: UIViewController) {
        let vc = UIStoryboard.getViewController(id: "PatientReviewViewController") as! PatientReviewViewController
        vc.patientId = patientId
        from.navigationController?.pushViewController(vc, animated: true)
    }
}
