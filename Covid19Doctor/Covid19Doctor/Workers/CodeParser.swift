//
//  CodeParser.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 15/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import Foundation
import RxSwift

class CodeParser {
    
    static let prefix = "covid-outbreak-control:"
    
    /// Parse the qr code string and extract the raw id.
    /// - Parameter string: the string in the qr code
    /// - Returns: the patient id
    static func extractPatientId(string: String) -> Single<String> {
        if let id = string.deletingPrefix(prefix) {
            return .just(id)
        } else {
            return .error(Errors.invalidPatientId)
        }
    }
    
    static func computeChecksum(patientId: String) -> String {
        let zero: Unicode.Scalar = "0"
        let nine: Unicode.Scalar = "9"
        
        let sum = patientId.unicodeScalars.reduce(0) { (result, letter) -> Int in
            let val = letter.value
            switch val {
            case zero.value...nine.value:
                return result + Int(val) - 48
            default:
                return result
            }
        }
        
        if let lastChar = "\(sum)".last {
            return String(lastChar)
        } else {
            return ""
        }
    }
    
    static func validateChecksum(publicPatientId: String) -> Bool {
        if let lastChar = publicPatientId.last {
            let patientId = String(publicPatientId.dropLast())
            return computeChecksum(patientId: patientId) == "\(lastChar)"
        } else {
            return false
        }
    }
    
    /// Append checksum at the end of the patient id.
    /// The result must be the only one to show to the user.
    /// - Parameter patientId: the patient id
    /// - Returns: the patient id + checksum
    static func computePublicPatientId(patientId: String) -> String {
        "\(patientId)\(computeChecksum(patientId: patientId))"
    }
    
    /// Remove checksum from safe patient id.
    /// The result is to be used internally and with the backend.
    /// - Parameter publicPatientId: the patient id with checksum
    /// - Returns: the patient id
    static func computePatientId(publicPatientId: String) -> String {
        String(publicPatientId.dropLast())
    }
}
