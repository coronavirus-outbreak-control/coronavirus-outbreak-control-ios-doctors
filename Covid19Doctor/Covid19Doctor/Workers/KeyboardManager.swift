//
//  KeyboardManager.swift
//  Covid19Doctor
//
//  Created by Francesco Cosentino on 14/04/2020.
//  Copyright © 2020 Francesco Cosentino. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import RxKeyboard

enum KeyboardPosition {
    case hidden
    case shown(CGFloat)
}

public final class KeyboardManager {
    
    public static let shared = KeyboardManager()
    private init() {
        enable()
    }
    
    private var bag = DisposeBag()
    private var viewBag = DisposeBag()
    private let position = BehaviorRelay<KeyboardPosition>(value: .hidden)
    
    private weak var targetView: UIView? {
        didSet {
            guard targetView != nil, viewToMove != nil else { return }
            
            viewBag = DisposeBag()
            position
                .subscribe(onNext: { [weak self] pos in
                    switch pos {
                    case .hidden:
                        self?.resetViewPosition()
                    case .shown(let keyboardTop):
                        self?.raiseView(keyboardTop: keyboardTop)
                    }
                })
                .disposed(by: viewBag)
        }
    }
    
    private weak var viewToMove: UIView?
    
    private func enable() {
        let frame = RxKeyboard.instance.frame.asObservable()
        let hidden = RxKeyboard.instance.isHidden.asObservable()
        
        Observable.combineLatest(frame, hidden, resultSelector: {
            $1 ? KeyboardPosition.hidden : .shown($0.origin.y)
        })
            .bind(to: position)
            .disposed(by: bag)
    }
    
    public func startKeepingViewVisible(_ target: UIView, viewToMove: UIView) {
        if self.targetView != nil, target != self.targetView {
            resetViewPosition()
        }
        
        self.viewToMove = viewToMove
        self.targetView = target
    }
    
    public func stopKeepingViewVisible() {
        self.targetView = nil
        self.viewToMove = nil
        self.viewBag = DisposeBag()
    }
    
    
    // MARK: - Move views
    
    private func raiseView(keyboardTop: CGFloat) {
        guard let target = targetView else { return }
        let targetBottom = target.convert(CGPoint(x: 0, y: target.bounds.size.height),
                                          to: nil).y
        let diff = targetBottom - keyboardTop
        if diff > 0 {
            UIView.animate(withDuration: 0.23) {
                self.viewToMove?.frame.origin.y = -diff
            }
        }
    }
    
    private func resetViewPosition() {
        UIView.animate(withDuration: 0.23) {
            self.viewToMove?.frame.origin.y = 0
        }
    }
}
